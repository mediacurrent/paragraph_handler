<?php

/**
 * @file
 * Contains paragraph_handler.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;

/**
 * Implements hook_help().
 */
function paragraph_handler_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the paragraph_handler module.
    case 'help.page.paragraph_handler':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Allows paragraphs to controlled by classes.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_preprocess_paragraph().
 */
function paragraph_handler_preprocess_paragraph(&$variables) {
  if (empty($variables['paragraph']) || $variables['view_mode'] == 'preview') {
    return;
  }

  $paragraph = $variables['paragraph'];

  if (!$plugin = paragraph_handler_get_handler($paragraph)) {
    return;
  }

  $plugin->dispatchPreprocess($paragraph, $variables);
}

/**
 * Implements hook_ENTITY_TYPE_view_alter().
 */
function paragraph_handler_paragraph_view_alter(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display) {

  // Delegate paragraph display to a plugin class.
  if ($display->getMode() == 'preview' || !$plugin = paragraph_handler_get_handler($paragraph)) {
    return;
  }

  $plugin->dispatchBuild($paragraph, $build);
}

/**
 * Get the correct ParagraphHandler plugin for a given paragraph.
 *
 * @param Drupal\paragraphs\Entity\Paragraph $paragraph
 *   A paragraph entity.
 *
 * @return mixed
 *   Null or a ParagraphHandler plugin.
 */
function paragraph_handler_get_handler(Paragraph $paragraph) {
  $plugin_manager = \Drupal::service('plugin.manager.paragraph_handler');
  $definitions = $plugin_manager->getDefinitions();

  foreach ($definitions as $plugin_id => $definition) {
    if ($plugin_id == $paragraph->bundle()) {
      return $plugin_manager->createInstance($plugin_id);
    }
  }
}
